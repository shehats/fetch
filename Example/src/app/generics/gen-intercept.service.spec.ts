import { TestBed, inject } from '@angular/core/testing';

import { GenInterceptService } from './gen-intercept.service';

describe('GenInterceptService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenInterceptService]
    });
  });

  it('should be created', inject([GenInterceptService], (service: GenInterceptService) => {
    expect(service).toBeTruthy();
  }));
});
