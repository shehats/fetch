import { TestBed, inject } from '@angular/core/testing';

import { ToPromiseConcreteService } from './to-promise-concrete.service';

describe('ToPromiseConcreteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToPromiseConcreteService]
    });
  });

  it('should be created', inject([ToPromiseConcreteService], (service: ToPromiseConcreteService) => {
    expect(service).toBeTruthy();
  }));
});
