import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Post } from '../post/Post';

@Injectable()
export class ToPromiseSpecService {
  private actionUrl: string;

  constructor(private http: Http) {
    this.actionUrl = 'https://jsonplaceholder.typicode.com/posts';
  }

  public fetch <T extends Post> (url: string): Promise<T> {
    return this.http.get(this.actionUrl + '/' + url)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }

  public fetchAll <T extends Post> (url: string): Promise<T[]> {
    return this.http.get(this.actionUrl + '/' + url)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }


  public fetchById <T extends Post> (url: string, id: number): Promise<T> {
    return this.http.get(this.actionUrl + '/' + url + '/' +id)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));             
  }

  public save <T extends Post> (url: string, obj: T): Promise<T> {
    return this.http.post(this.actionUrl + '/' + url, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }

  public update <T extends Post> (url: string, obj: T) {
    return this.http.put(this.actionUrl + '/' + url, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }

  public updateById <T extends Post> (url: string, id: number, obj: T): Promise<T> {
    return this.http.put(this.actionUrl + '/' + url + '/' + id, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));   
  }

  public deleteObj <T extends Post> (obj: T): Promise<T> {
    return this.http.delete(this.actionUrl, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama')); 
  }

  public deleteById <T extends Post> (url: string, id: number): Promise<T> {
    return this.http.delete(this.actionUrl + '/' + url + '/' +id)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama')); 
  }
}
