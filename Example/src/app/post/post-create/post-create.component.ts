import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { PostService } from '../post.service';
import { Post } from '../Post';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css'],
  providers: [PostService]
})

export class PostCreateComponent implements OnInit, OnDestroy {
  id: number;
  post: Post;
  postForm: FormGroup;
  private sub: any;

  constructor (private route: ActivatedRoute,
               private router: Router,
               private postService: PostService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });

    this.postForm = new FormGroup({
      title: new FormControl('', Validators.required),
      body: new FormControl('', Validators.required)
    });
  }

  ngOnDestroy (): void {
    this.sub.unsubscribe();
  }

  onSubmit () {
    if (this.postForm.valid) {
      if (this.id) {
      let post: Post = new Post(null, this.id,
                               this.postForm.controls['title'].value,
                               this.postForm.controls['body'].value);
      this.postService.updatePost(post).subscribe();
    } else {
      let post: Post = new Post(null, null,
                               this.postForm.controls['title'].value,
                               this.postForm.controls['body'].value);
      this.postService.savePost(post).subscribe();
    }
    this.postForm.reset();
    this.router.navigate(['/']);
    }
  }

  redirectPostPage () {
    this.router.navigate(['/']);
  }
}
