import { Component, OnInit } from '@angular/core';
import { Post } from '../Post';
import { PostService } from '../post.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  providers: [PostService]
})
export class PostListComponent implements OnInit {
  posts: Post[];

  constructor (private router: Router,
                private postService: PostService) { }

  ngOnInit() {
  	this.getAllPosts();
  }

  getAllPosts () {
  	this.postService.findAll().subscribe(
  			posts => {
  				this.posts = posts;
  			},
  			err => {
  				console.log(err);
  				console.log('Kill me now');
  			}
  		)
  }

  newPost () {
    this.router.navigate(['/create']);
  }

  editPost (post: Post) {
    if (post)
      this.router.navigate(['/edit', post.id]);
  }

  deletePost (post: Post) {
    if (post) {
      this.postService.deletePost(post.id).subscribe(
        res => {
          this.getAllPosts();
          this.router.navigate(['/']);
          console.log('Cry mama is dead also ' || res);
        }
       );
    }
  }
}
