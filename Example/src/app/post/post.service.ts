import { Injectable } from '@angular/core';
import { Post } from './Post';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PostService {
	private apiUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private http: Http) {
  }

  findAll <T extends Post> () : Observable<T[]> {
  	return this.http.get(this.apiUrl)
  							.map((res: Response) => res.json())
  							.catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  findById (id: number): Observable<Post> {
  	return this.http.get(this.apiUrl + '/' +id)
                .map((res: Response) => res.json())
                .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  savePost <T extends Post> (post: T): Observable<T> {
  	return this.http.post(this.apiUrl, post)
                .map((res: Response) => res.json())
                .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  deletePost (id: number): Observable<boolean> {
  	return this.http.delete(this.apiUrl+'/'+id)
                .map((res: Response) => res.json())
                .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  updatePost <T extends Post> (post: T): Observable<T> {
  	return this.http.put(this.apiUrl, post)
                .map((res: Response) => res.json())
                .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }
}
