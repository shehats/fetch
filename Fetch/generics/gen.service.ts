import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GenService {
  private actionUrl: string;

  constructor(private http: Http) {
  	this.actionUrl = 'https://jsonplaceholder.typicode.com/posts';
  }

  public fetch <T> (url: string): Observable<T> {
    return this.http.get(this.actionUrl + '/' + url)
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  public fetchAll <T> (url: string): Observable<T[]> {
  	return this.http.get(this.actionUrl + '/' + url)
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }


  public fetchById <T> (url: string, id: number): Observable<T> {
  	return this.http.get(this.actionUrl + '/' + url + '/' +id)
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));           	
  }

  public save <T> (url: string, obj: T): Observable<T> {
    return this.http.post(this.actionUrl + '/' + url, JSON.stringify(obj))
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  public update <T> (url: string, obj: T) {
    return this.http.put(this.actionUrl + '/' + url, JSON.stringify(obj))
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));
  }

  public updateById <T> (url: string, id: number, obj: T): Observable<T> {
    return this.http.put(this.actionUrl + '/' + url + '/' + id, JSON.stringify(obj))
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama'));    
  }

  public deleteObj <T> (obj: T): Observable<T> {
    return this.http.delete(this.actionUrl, JSON.stringify(obj))
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama')); 
  }

  public deleteById <T> (url: string, id: number): Observable<T> {
    return this.http.delete(this.actionUrl + '/' + url + '/' +id)
    .map((res: Response) => res.json())
    .catch((err: any) => Observable.throw(err.json().error || 'Cry Mama')); 
  }

}
