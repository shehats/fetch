import { TestBed, inject } from '@angular/core/testing';

import { HttpClientExampleService } from './http-client-example.service';

describe('HttpClientExampleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HttpClientExampleService]
    });
  });

  it('should be created', inject([HttpClientExampleService], (service: HttpClientExampleService) => {
    expect(service).toBeTruthy();
  }));
});
