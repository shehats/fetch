import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Post } from '../post/Post';

@Injectable()
export class ToPromiseConcreteService {
	private actionUrl: string;

  constructor(private http: Http) {
    this.actionUrl = 'https://jsonplaceholder.typicode.com/posts';
  }

  public fetch (url: string): Promise<Post> {
    return this.http.get(this.actionUrl + '/' + url)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }

  public fetchAll (url: string): Promise<Post[]> {
    return this.http.get(this.actionUrl + '/' + url)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }


  public fetchById (url: string, id: number): Promise<Post> {
    return this.http.get(this.actionUrl + '/' + url + '/' +id)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));             
  }

  public save (url: string, obj: T): Promise<Post> {
    return this.http.post(this.actionUrl + '/' + url, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }

  public update (url: string, obj: Post) {
    return this.http.put(this.actionUrl + '/' + url, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));
  }

  public updateById (url: string, id: number, obj: Post): Promise<Post> {
    return this.http.put(this.actionUrl + '/' + url + '/' + id, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama'));   
  }

  public deleteObj (obj: T): Promise<Post> {
    return this.http.delete(this.actionUrl, JSON.stringify(obj))
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama')); 
  }

  public deleteById <Post> (url: string, id: number): Promise<Post> {
    return this.http.delete(this.actionUrl + '/' + url + '/' +id)
    .toPromise().then((res: Response) => res.json())
    .catch((err: any) => console.log(err.json().error || 'Cry Mama')); 
  }
}
