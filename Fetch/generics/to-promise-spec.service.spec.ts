import { TestBed, inject } from '@angular/core/testing';

import { ToPromiseSpecService } from './to-promise-spec.service';

describe('ToPromiseSpecService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToPromiseSpecService]
    });
  });

  it('should be created', inject([ToPromiseSpecService], (service: ToPromiseSpecService) => {
    expect(service).toBeTruthy();
  }));
});
