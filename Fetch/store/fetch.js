export function authenticateUser (http, path, authParams){
  return new Promise ((resolve, reject) => {
    http.post(`${path}`,authParams)
    .then((response) => {
      resolve(response.headers)
    })
    .catch((error) => {
      reject(error)
    })
  }
  )
}

export function fetch (http, path){
  return new Promise((resolve, reject) => {
    http.get(`${path}`)
    .then((response) => {
      console.log(response)
      resolve(response.data)
    })
    .catch((error) => {
      reject(error)
    })
  })
}

export function fetchItem (http, path, id) {
  return fetch(http,`${path}/${id}`)
}

export function fetchItems (http, path, ids) {
  return Promise.all(ids.map(id => fetchItem(http, path, id.id)))
}

export function fetchDetail (http, path, id, key) {
  return fetch(http,`${path}/${id}/${key}`)
}