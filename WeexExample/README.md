# weex

## make sure you have weex dev tools 
npm install -g weex-toolkit

## getting started

```bash
npm install
```

## npm scripts

```bash
# build both two js bundles for Weex and Web
npm run build

# build the two js bundles and watch file changes
npm run dev

# start a Web server at 1337 port
npm run serve

# start weex-devtool for debugging with native
npm run debug
```


# Andriod

## Setup SDK for android
open android studio

download weex sdk from https://bintray.com/alibabaweex/maven/weex_sdk/view

unzip the sdk to the project folder "android"

connect your device and run it

# IOS

open XCode

go https://cocoapods.org/pods/WeexSDK

install the sdk to the project folder "ios"

connect your device and run it

# Andriod deployment

https://developer.android.com/distribute/best-practices/launch/index.html

# IOS deployment

https://developer.apple.com/library/content/documentation/IDEs/Conceptual/AppDistributionGuide/Introduction/Introduction.html

and hope Apple approves it

# Have fun
