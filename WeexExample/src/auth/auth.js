let axios = require('weex-axios')

export const http = {
	create (baseURL) {
		return axios.create({
			baseURL: `${baseURL}`,
			headers: {
			}
		})
	},
	set (instance,headers) {
		instance.defaults.headers['access-token'] = headers['access-token'],
		instance.defaults.headers['token-type'] = headers['token-type'],
		instance.defaults.headers['client'] = headers['client'],
		instance.defaults.headers['uid'] = headers['uid'],
		instance.defaults.headers['expiry'] = headers['expiry']	
	},
	unset (instance,logoutUrl) {
		return instance.delete(`${logoutUrl}`)
	}
}
