const auth = {
	install(Vue, options) {
		let auth = require('./auth.js')
		let vm = auth.http
		let store = options.store
		let instance = vm.create(options.baseUrl)
		let router = options.router
		var authenicated = false

		var intializeData = () => {
			var usr = store.getters.userData
			if (!usr && store.getters.authParams) {
				store.dispatch('VALIDATE_TOKEN', {
					http: instance,
					path: options.validateUrl
				})
				.catch(err => {
					login(store.getters.authParams)
				})
			}else if (usr) {
				vm.set(instance,usr)
			}
		}

		store.commit('INIT_STATE', {callback: intializeData})

		var login = (authParams) => {
			store.dispatch('FETCH_USER',{
				http: instance,
				authUrl: options.authUrl,
				authParams:authParams
			})
			.then((usr) => {
				vm.set(instance,usr)
			})
			.then(() => {
				store.commit('SET_USER',
				{
					headers: instance.defaults.headers, 
					authParams: authParams
				})
			})
		}

		Vue.mixin({
			data () {
				return {
					http: instance
				}
			},
			computed: {
				isAuthenticated () {
					return authenicated
				}
			},
			methods: {
				authenticate (authParams) {
					login(authParams)
				},
				unauthenticate () {
					vm.unset(instance,options.logoutUrl).
					then(() => {
						authenicated = false
						router.push('/login')
					}).then(() => {
						store.commit('UNSET_USER')
					})
				}
			}
		})
	}
}

export default auth
