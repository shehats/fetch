import App from './App.vue'
import router from './router'
import store from './store'
import {sync} from 'vuex-router-sync'
import * as filters from './filters'
import mixins from './mixins'
import axios from 'weex-axios'
import auth from './auth'
import includes from './include'
import {BASE_URL, LOG_IN_URL, LOG_OUT_URL, VALID_URL} from './routes'

sync(store, router)

Object.keys(filters).forEach(key => {
	Vue.filter(key, filters[key])
})

Vue.mixin(mixins)
Vue.mixin(includes)

Vue.use(auth, {
	baseUrl: BASE_URL,
	authUrl: LOG_IN_URL,
	logoutUrl: LOG_OUT_URL,
	validateUrl: VALID_URL,
	store: store,
	router: router
})

new Vue(Vue.util.extend({ el: '#root', router, store }, App))

router.push('/')
