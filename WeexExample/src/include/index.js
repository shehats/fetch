const modal = weex.requireModule('modal')

export default {
	components: {
		button: require('./button.vue'),
		h1: require('./h1.vue'),
		h2: require('./h2.vue'),
		h3: require('./h3.vue'),
		hn: require('./hn.vue'),
		listItem: require('./list-item.vue'),
		marquee: require('./marquee.vue'),
		panel: require('./panel.vue'),
		tip: require('./tip.vue'),
		sliderItem: require('./slider-item.vue'),
		sliderPage: require('./slider-page.vue'),
		tabbar: require('./tabbar.vue')
	},
	methods: {
		showToast (content, duration) {
			modal.toast({
				message: content,
				duration: duration
			})
		},
		showAlert (content,duration,callback) {
			modal.alert({
				message: content,
				duration: duration
			}, callback())
		},
		showConfirm (content, duration, callback) {
			modal.confirm({
				message: content,
				duration: duration
			}, callback())
		},
		showPrompt (content, duration, callback) {
			modal.prompt({
				message: content,
				duration: duration
			}, callback())
		}
	}
}