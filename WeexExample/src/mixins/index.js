let modal = require('./modal.js')
export default {
	data () {
		return {
			modal: modal.Toast
		}
	},
	methods: {
		jump (to) {
			if (this.$router) {
				this.$router.push(to)
			}
		},
		back () {
			this.$router.go(-1)
		},
		getName (url, story, id) {
			var name = []

			if (url.slice(1) === 'programs')
				name.push({key: '', val:story.name})
			else if (url.slice(1) === 'invoices') {
				name.push({key: 'Program name: ', val: story.program.name}) 
				name.push({key:'total price: ', val: story.total_price}) 
				name.push({key: 'total paid: ', val: story.total_paid})
			}
			else if (url.match(/programs\/\d+/)) {
				name.push({key: 'Date: ', val: story.departure_date})
			}
			else if (url.match(/trips\/\d+/)) {
				if (story.phone) {
					name.push({key: 'Name: ', val: story.name})
					name.push({key: 'Phonenumber: ', val: story.phone})
					name.push({key: 'National ID: ', val: story.national_id})
				}
				else if (story.total_price) {
					name.push({key: 'Main contact name: ', val: story.main_contact.name})
					name.push({key: 'Main contact phonenumber: ', val: story.main_contact.phone})
					name.push({key: 'Main contact ID: ', val: story.main_contact.national_id}) 
					name.push({key: 'Total price: ', val: story.total_price}) 
					name.push({key: 'Status: ', val: story.status})
				}
			}
			return name
		}
	},
	beforeDestroy () {
		this.$store.dispatch('SAVE_STATE')
	}
}
