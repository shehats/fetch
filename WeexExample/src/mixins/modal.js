let modal = weex.requireModule('modal')
export const Toast = {
  showToast (msg) {
    modal.toast({
      message: msg,
      duration: 0.3
    })
  },
  showAlert (msg, callback) {
    modal.alert({
      message: msg,
      duration: 0.3
    },
      callback()
    )
  },
  showConfirm (msg, callback) {
    modal.confirm({
      message: msg,
      duration: 0.3
    },
      callback()
    )
  },
  showPrompt (msg, callback) {
    modal.prompt({
      message: msg,
      duration: 0.3
    },
      callback()
    )
  }
}
