import Router from 'vue-router'
import StoriesView from './views/StoriesView.vue'
import LoginView from './views/LoginView.vue'
import DetailView from './views/DetailView.vue'
import NotFoundView from './views/NotFoundView.vue'
import { PROGRAMS_URL, INVOICES_URL, TRIPS_URL, TRAVELLERS_URL } from './routes'

Vue.use(Router)

function createStoriesView (type, keys) {
  return {
    name: `${type}-stories-view`,
    render (createElement) {
      return createElement(StoriesView, { props: { type: type, keys: keys }})
    }
  }
}

function createStoriesDetailsView (type, keys) {
  return {
    name: `${type}-detail-view`,
    render (createElement) {
      return createElement(DetailView, { props: {type: type, keys: keys} })
    }
  }
}

export default new Router({
  // mode: 'abstract',
  routes: [
    { path: '/programs', 
      component: createStoriesView('programs', [TRIPS_URL]), 
      auth: true
    },
    { path: '/invoices', 
      component: createStoriesView('invoices',[]),
      auth: true
    },
    { path: '/login', 
      component: LoginView
    },
    { path: '/programs/:id(\\d+)', 
      component: createStoriesDetailsView('trips', [TRIPS_URL]),
      auth: true
    },
    { path: '/trips/:id(\\d+)', 
      component: createStoriesDetailsView('travelers', [TRAVELLERS_URL,INVOICES_URL]),
      auth: true
    },
    { path: '/', 
      redirect: '/programs',
      auth: true
    },
    {
      path: '*',
      component: NotFoundView
    }
  ]
})
