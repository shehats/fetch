export default function() {
	return '<div class="row">\
	<hr class="hr-warning" style="margin-top: 75px;"/>\
	<div class="portlet">\
	<div class="portlet-title">\
	<div class="caption caption-red">\
	<i class="glyphicon glyphicon-pushpin"></i>\
	<span class="caption-subject bold font-yellow-crusta uppercase">Tabs </span>\
	<span class="caption-helper">more samples...</span>\
	</div>\
	<ul class="nav nav-tabs">\
	<li>\
	<a href="#portlet_tab3" data-toggle="tab">Tab 3 </a>\
	</li>\
	<li>\
	<a href="#portlet_tab2" data-toggle="tab">Tab 2 </a>\
	</li>\
	<li class="active">\
	<a href="#portlet_tab1" data-toggle="tab">Tab 1 </a>\
	</li>\
	</ul>\
	</div>\
	<div class="portlet-body">\
	<div class="tab-content">\
	<div class="tab-pane active" id="portlet_tab1">\
	<h4>About</h4>\
	<p></p>\
	</div>\
	<div class="tab-pane" id="portlet_tab2">\
	<h4>More About</h4>\
	<p></p>\
	</div>\
	<div class="tab-pane" id="portlet_tab3">\
	<h4>TMore About</h4>\
	<p></p>\
	</div>\
	</div>\
	</div>\
	</div>\
	</div>' 
}