export default {
  logout (url) {
    var headers = new Headers()
    var cred = { method: 'DELETE',
                headers: headers,
                mode: 'cors',
                credentials: 'same-origin',
                cache: 'default' }

    return new Promise((resolve, reject) => {
      fetch(url, cred)
      .then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  },
  get (url) {
    var headers = new Headers()
    var cred = { method: 'GET',
                headers: headers,
                credentials: 'same-origin',
                mode: 'cors',
                cache: 'default' }

    return new Promise((resolve, reject) => {
      fetch(url, cred)
      .then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  },
  delete (url, data) {
    var headers = new Headers()
    console.log(data)
    var cred = { method: 'DELETE',
                body: JSON.stringify(data),
                headers: headers,
                mode: 'cors',
                credentials: 'same-origin',
                cache: 'default' }

    return new Promise((resolve, reject) => {
      fetch(url, cred)
      .then(res => {
        resolve(res.json())
      }).catch(err => {
        reject(err)
      })
    })
  },
  post(url, data) {
    console.log(data)
    var headers = new Headers({
      'Content-Type': 'application/json'
    })
    var cred = { method: 'POST',
                body: JSON.stringify(data),
                headers: headers,
                mode: 'cors',
                credentials: 'same-origin',
                cache: 'default' }

    return new Promise((resolve, reject) => {
      fetch(url, cred)
      .then(res => {
        resolve(res.json())
      }).catch(err => {
        reject(err)
      })
    })
  },
  check(url, data) {
    var headers = new Headers({
      'Content-Type': 'application/json'
    })
    var cred = { method: 'POST',
                body: JSON.stringify(data),
                headers: headers,
                mode: 'cors',
                credentials: 'same-origin',
                cache: 'default' }

    return new Promise((resolve, reject) => {
      fetch(url, cred)
      .then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  },
  put (url, data) {
    console.log(data)
    var headers = new Headers({
      'Content-Type': 'application/json'
    })
    console.log(data)
    var cred = { method: 'PUT',
                body: JSON.stringify(data),
                headers: headers,
                mode: 'cors',
                credentials: 'same-origin',
                cache: 'default' }

    return new Promise((resolve, reject) => {
      fetch(url, cred)
      .then(res => {
        resolve(res.json())
      }).catch(err => {
        reject(err)
      })
    })
  },
  makeBlob (path) {
    return new Promise((resolve, reject) => {
      return fetch(path).then(res => {
        resolve(res.blob())
      })  
    })
  }
}

