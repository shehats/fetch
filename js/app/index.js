import actions from './api';
import router from './router';
import nav from './nav';
import login from './login'
import profile from './profile';

$(function () { 
  actions.get('http://localhost:9000/Reimbursement/api/v1/validate')
        .then(val => {
          if (val.status >= 400)
            login.authenticate(val)
          else {
            val.json().then(x => {
              profile.load(x)
            })
          }
        })
        .catch(err => console.log(err))

  
  // $('#view').html(router.redirect({'url': 'login'}));
  $(".nav-item").click(function (e) {
    $(".nav-item").removeClass("active");
    console.log($(this).attr('id'))
    $(this).addClass("active");
    console.log($(this).attr('id'));
    $('#view').html(router.redirect({'url': $(this).attr('id')}))
    // location.reload();
  })

});

