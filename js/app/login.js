import actions from './api';
import profile from './profile';
import nav from './nav';
import router from './router';

export default {
  authenticate(res) {
    if (res.status >= 400) {
      $('#navbarCollapse1').html(nav(false))
      $('#view').html('<div id="curn" class="row" style="margin-top: 20%;>\
        <div class="col-xs-6 col-md-offset-3">\
        <div class="panel panel-login" style="margin-top: 175px;">\
        <div class="panel-heading">\
        <div class="row">\
        <div class="col-xs-6">\
        <a href="#" class="active" id="login-form-link">Login</a>\
        </div>\
        <div class="col-xs-6">\
        <a href="#" id="register-form-link">Register</a>\
        </div>\
        </div>\
        <hr>\
        </div>\
        <div class="panel-body">\
        <div class="row">\
        <div class="col-sm-12">\
        <div id="login-form" style="display: block;">\
        <div class="form-group">\
        <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">\
        </div>\
        <div class="form-group">\
        <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">\
        </div>\
        <div class="form-group">\
        <div class="row">\
        <div class="col-sm-6 col-sm-offset-3">\
        <button id="login-submit" tabindex="4" class="form-control btn btn-login">Log In</button>\
        </div>\
        </div>\
        </div>\
        <div class="form-group">\
        <div class="row">\
        <div class="col-lg-12">\
        </div>\
        </div>\
        </div>\
        </div>\
        <div id="register-form" style="display: none;">\
        <div class="form-group">\
        <input type="text" name="username" id="rusername" tabindex="1" class="form-control" placeholder="Username" value="">\
        </div>\
        <div class="form-group">\
        <input type="text" name="firstName" id="firstName" tabindex="1" class="form-control" placeholder="First name" value="">\
        </div>\
        <div class="form-group">\
        <input type="text" name="lastName" id="lastName" tabindex="1" class="form-control" placeholder="Last name" value="">\
        </div>\
        <div class="form-group">\
        <input type="email" name="email" id="remail" tabindex="1" class="form-control" placeholder="Email Address" value="">\
        </div>\
        <div class="form-group">\
        <input type="password" name="password" id="rpassword" tabindex="2" class="form-control" placeholder="Password">\
        </div>\
        <div class="form-group">\
        <input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">\
        </div>\
        <div class="form-group">\
        <div class="row">\
        <div class="col-sm-6 col-sm-offset-3">\
        <button id="register-submit" tabindex="4" class="form-control btn btn-register">Register</button>\
        </div>\
        </div>\
        </div>\
        <div id="error" class="col-sm-6 col-sm-offset-3" role="alert">\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>')
    }

    $(function () {
      $('#login-form-link').click(function (e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
      });



      $('#register-form-link').click(function (e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
      });

      $('#login-submit').click(function(e) {
        var data = {
          'username': $('#username').val(),
          'password': $('#password').val()
        }

        actions.post('http://localhost:9000/Reimbursement/api/v1/login', data)
        .then(val => profile.load(val))
        .catch(err => console.log(err))
      });

      $('#register-submit').click(function(e) {
        var data = {
          'username': $('#rusername').val(),
          'password': $('#rpassword').val(),
          'firstName': $('#firstName').val(),
          'lastName': $('#lastName').val(),
          'email': $('#remail').val(),
          'role': '3'
        }

        actions.post('http://localhost:9000/Reimbursement/api/v1/register', data)
        .then(val => profile.load(val))
        .catch(err => console.log(err))
      });

      $('#rusername').blur(e => {
        actions.check('http://localhost:9000/Reimbursement/api/v1/check', 
            {'username': $('#rusername').val()}).then(val => {
                if (val.status >= 400)
                    $('#error').html('<strong>Username already exist</strong>')
                else
                    $('#error').html('')
            })
      });

      $('#confirm-password').blur(e => {
        if ($('#Rpassword').val() != $('#confirm-password').val())
            $('#error').html('<div class="alert alert-danger"><strong>Passwords should match</strong></div>')
        else
            $('#error').html('')
      })

      $(".nav-item").click(function (e) {
        $(".nav-item").removeClass("active");
        console.log($(this).attr('id'))
        $(this).addClass("active");
        console.log($(this).attr('id'));
        $('#view').html(router.redirect({'url': $(this).attr('id')}))
        // location.reload();
      })

      
    })
  }
}
