import actions from './api';
import nav from './nav';
import managerAdmin from './managerAdmin';
import router from './router';

export default {
  load(data) {
    console.log('fdddfkfkdkfdk')
    $('#navbarCollapse1').html(nav(true));
    $('#view').html('<div class="row">\
      <div class="portlet">\
      <div class="portlet-title">\
      <div class="caption caption-red">\
      <i class="glyphicon glyphicon-grain"></i>\
      <span class="caption-subject bold font-yellow-crusta uppercase">'+data.user.firstName+' '+ data.user.lastName
      +' (Manager)<a id="beAdmin" class="glyphicon glyphicon-indent-right text-success"></a></span></div>\
      <span class="col-md-4 col-md-offset-3">\
      <span action="" class="search-form">\
      <span class="form-group has-feedback">\
      <label for="search" class="sr-only">Search</label>\
      <input type="text" class="form-control" name="search" id="search" placeholder="search">\
      <span class="glyphicon glyphicon-search text-warning form-control-feedback"></span>\
      </span></span></span>\
      <ul class="nav nav-tabs">\
      <li id="all" class="filter active">\
      <a href="#portlet_tab1" data-toggle="tab">All</a>\
      </li>\
      <li id="accepted" class="filter">\
      <a href="#portlet_tab2" data-toggle="tab">Admin</a>\
      </li>\
      </ul>\
      <a data-toggle="modal" data-target="#configModal" class="glyphicon glyphicon-cog text-warning"></a>\
      <a data-toggle="modal" data-target="#emailModal" class="glyphicon glyphicon-envelope text-danger"></a>\
      </div>\
      <div class="col-xs-9 border" style="margin-top: 25px;">\
      <div class="category">\
      <div class="tab-content">\
      <div class="tab-pane active" id="portlet_tab1">\
      </div>\
      <div class="tab-pane active" id="portlet_tab2">\
      </div>\
      </div>\
      </div>\
      </div>\
      </div>\
      </div>')

    // users
    $(function () {
      var all = data['users'];
      var admin = _.filter(all, el => el.role == 2)
      var updated;
      var deleted;

      function insert (el, lis) {
        $(el).html('')
        _.each(lis, val => $(el).append('<div><p>'+val.firstName+ ' ' + val.lastName+'</p><span>\
          <a data-toggle="modal" data-target="#reimUserModal"  id="'+ val.id +'" class="mUser glyphicon glyphicon-option-horizontal text-warning"></a>\
          <a data-toggle="modal" data-target="#deleteUserModal"  id="'+ val.id +'"class="mUserDelete glyphicon glyphicon-trash text-danger"></a>\
          </span></div>')) 
      }

      function init () {
        insert('#portlet_tab1', all)
        insert('#portlet_tab2', admin)
      }

      init()

      $('#search').keypress(e => {
        if ($('#search').val() != '') {
          var searchAll = _.filter(all, val => (val.firstName.includes($('#search').val()) || val.lastName.includes($('#search').val()) || val.username.includes($('#search').val())))
          var searchAdmin = _.filter(admin, val => (val.firstName.includes($('#search').val()) || val.lastName.includes($('#search').val()) || val.username.includes($('#search').val())))
          insert('#portlet_tab1', searchAll)
          insert('#portlet_tab2', searchAccepted)
        }
        else
          init()
      })

      $('#beAdmin').click(function (e) {
        managerAdmin.load(data)
      })

      $('.mUser').click(e => {
        var id = e.target.id;
        updated = _.find(all, val => (val.id == id))
      })

      $('.mUserDelete').click(e => {
        var id = e.target.id;
        deleted = _.find(all, val => (val.id == id))
      })

      $('#mkadmin').click(e => {
        updated.role = 2;
        actions.put('http://localhost:9000/Reimbursement/api/v1/users', updated)
          .then(() => location.reload())
          .catch(e => console.log(e))
      })

      $('#mkuser').click(e => {
        updated.role = 3;
        actions.put('http://localhost:9000/Reimbursement/api/v1/users', updated)
          .then(() => location.reload())
          .catch(e => console.log(e))
      })

      $('#deleteUser').click(e => {
        actions.delete('http://localhost:9000/Reimbursement/api/v1/users', deleted)
          .then(() => location.reload())
          .catch(e => console.log(e))
      })

      $('#logout').click(function (e) {
        actions.logout('/Reimbursement/api/v1/logout')
        .then(() => location.reload())
        .catch(err => console.log(err))
      })

      $(".nav-item").click(function (e) {
        $(".nav-item").removeClass("active");
        console.log($(this).attr('id'))
        $(this).addClass("active");
        console.log($(this).attr('id'));
        $('#view').html(router.redirect({'url': $(this).attr('id')}))
        // location.reload();
      })

    })
  }
}