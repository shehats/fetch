import actions from './api';
import nav from './nav';
import manager from './manager';
import router from './router';

export default {
  load (data) {
    $('#navbarCollapse1').html(nav(true));
    $('#view').html('<div class="row" style="margin-top: 20%">\
      <div class="portlet">\
      <div class="portlet-title">\
      <div class="caption caption-red">\
      <i class="glyphicon glyphicon-grain"></i>\
      <span class="caption-subject bold font-yellow-crusta uppercase">'+data.user.firstName+' '+ data.user.lastName
      + '  ' +' (Manager)<a id="beManager" class="glyphicon glyphicon-indent-right text-success"></a></span></div>\
      <span class="col-md-4 col-md-offset-3">\
      <span action="" class="search-form">\
      <span class="form-group has-feedback">\
      <label for="search" class="sr-only">Search</label>\
      <input type="text" class="form-control" name="search" id="search" placeholder="search">\
      <span class="glyphicon glyphicon-search text-warning form-control-feedback"></span>\
      </span></span></span>\
      <ul class="nav nav-tabs">\
      <li id="all" class="filter active">\
      <a href="#portlet_tab1" data-toggle="tab">All</a>\
      </li>\
      <li id="accepted" class="filter">\
      <a href="#portlet_tab2" data-toggle="tab">Accepted</a>\
      </li>\
      <li id="rejected" class="filter">\
      <a href="#portlet_tab3" data-toggle="tab">Rejected</a>\
      </li>\
      <li id="pending" class="filter">\
      <a href="#portlet_tab4" data-toggle="tab">Pending</a>\
      </li>\
      </ul>\
      <a data-toggle="modal" data-target="#configModal" class="glyphicon glyphicon-cog text-warning"></a>\
      <a data-toggle="modal" data-target="#emailModal" class="glyphicon glyphicon-envelope text-danger"></a>\
      </div>\
      <div class="col-xs-3 border col-xs-offset-0" style="margin-top: 25;">\
      <h3 class="side-title">Category</h3>\
      <ul class="single list-unstyled">\
      <li class="mock filter" id="#all"><a>All</a></li>\
      <li class="mock" id="#business"><a>Business travel</a></li>\
      <li class="mock" id="#training"><a>Education or training</a></li>\
      <li class="mock" id="#supplies"><a>Business supplies</a></li>\
      <li class="mock" id="#tools"><a>Business tools</a></li>\
      <li class="mock" id="#miscellaneous"><a>Miscellaneous Expenses</a></li>\
      <li class="mock" id="#medical"><a>Medical Expenses</a></li>\
      </ul>\
      </div>\
      <div class="col-xs-9 border" style="margin-top: 25px;">\
      <div class="category" style="overflow: scroll;">\
      <div class="tab-content pre-scrollable">\
      <div class="tab-pane active" id="portlet_tab1">\
      </div>\
      <div class="tab-pane active" id="portlet_tab2">\
      </div>\
      <div class="tab-pane active" id="portlet_tab3">\
      </div>\
      <div class="tab-pane active" id="portlet_tab4">\
      </div>\
      </div>\
      </div>\
      </div>\
      </div>\
      </div>')

    $(function () {
      var all = data['pending'];
      var pending = _.filter(all, el => el.status == 'pending')
      var accepted = _.filter(all, el => el.status == 'accepted')
      var rejected = _.filter(all, el => el.status == 'rejected')
      var current = all;
      var curnAll = all;
      var curnPending = pending;
      var curnAccepted = accepted;
      var curnRejected = rejected;
      var updated;

      function insert (el, lis) {
        $(el).html('')
        _.each(lis, val => $(el).append('<div><p>'+val.reimbursement.description+'</p><span>\
          <a data-toggle="modal" data-target="#reimDecideModal"  id="'+val.reimbursement.id+'" class="mDecide glyphicon glyphicon-option-horizontal text-warning"></a>\
          </span></div>')) 
      }

      function filter (el, lis, type) {
        lis = _.filter(lis, val => (val.type == type))
        insert(el, lis)
      }

      function init () {
        insert('#portlet_tab1', all)
        insert('#portlet_tab2', accepted)
        insert('#portlet_tab3', rejected)
        insert('#portlet_tab4', pending)
      }
      function find (str) {
        if (str == 'all')
          init()
        else {
          curnRejected = _.filter(rejected, el => el.type == str)
          curnAccepted = _.filter(accepted, el => el.type == str)
          curnPending = _.filter(pending, el => el.type == str)
          curnAll = _.filter(all, el => el.type == str)
          insert('#portlet_tab1', curnAll)
          insert('#portlet_tab2', curnAccepted)
          insert('#portlet_tab3', curnRejected)
          insert('#portlet_tab4', curnPending)
        }
      }

      $('.mock').click(e => find(e.target.id))
      
      init()

      $('#search').keypress(e => {
        if ($('#search').val() != '') {
          var searchAll = _.filter(all, val => val.reimbursement.description == $('#search').val())
          var searchRejected = _.filter(curnRejected, val => val.reimbursement.description.includes($('#search').val()))
          var searchAccepted = _.filter(curnAccepted, val => val.reimbursement.description.includes($('#search').val()))
          var searchPending = _.filter(curnPending, val => val.reimbursement.description.includes($('#search').val()))
          insert('#portlet_tab1', searchAll)
          insert('#portlet_tab2', searchAccepted)
          insert('#portlet_tab3', searchRejected)
          insert('#portlet_tab4', searchPending)
        }
        else
          init()
      })

      $('.mDecide').click(e => {
        var id = e.target.id;
        updated = _.find(all, val => (val.reimbursement.id == id))
        $('#decideDesc').text('Description: ' + updated.reimbursement.description)
        $('#decideDescAuthor').text('Author: ' + updated.author)
        $('#decideStatus').text('Status: '+ updated.status)
        $('#decideSubmitted').text('At: ' + updated.reimbursement.submitted)
      })

      $('#beManager').click(e => {
        manager.load(data)
      })

      $('#descAccept').click(e => {
        updated.reimbursement.resolver = data.user.id;
        updated.reimbursement.status = 2;
        actions.put('http://localhost:9000/Reimbursement/api/v1/reimbursements', updated.reimbursement)
          .then(() => location.reload())
          .catch(err => console.log(err))
      })

      $('#descReject').click(e => {
        updated.reimbursement.resolver = data.user.id;
        updated.reimbursement.status = 3;
        actions.put('http://localhost:9000/Reimbursement/api/v1/reimbursements', updated.reimbursement)
          .then(() => location.reload())
          .catch(err => console.log(err))
      })

      $('#logout').click(function (e) {
        actions.logout('/Reimbursement/api/v1/logout')
        .then(() => location.reload())
        .catch(err => console.log(err))
      })

      $(".nav-item").click(function (e) {
        $(".nav-item").removeClass("active");
        console.log($(this).attr('id'))
        $(this).addClass("active");
        console.log($(this).attr('id'));
        $('#view').html(router.redirect({'url': $(this).attr('id')}))
        // location.reload();
      })

    })
  }
}