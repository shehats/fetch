export default function (authenticated) {
  const auth = '<ul class="navbar-nav navbar-right ml-auto">\
                          <li class="nav-item active" id="profile">\
                            <a class="nav-link">Profile <span class="sr-only"></span></a></li>\
                          <li class="nav-item" id="benefits"> <a class="nav-link">Benefits</a> </li>\
                          <li class="nav-item" id="about"> <a class="nav-link" >About</a> </li>\
                          <li class="nav-item glyphicon" id="logout"> <a class="nav-link glyphicon-log-out text-warning"></a></li>\
                      </ul>'
                      
  const unauth = '<ul class="navbar-nav navbar-right ml-auto">\
                    <li class="nav-item active" id="login">\
                      <a class="nav-link">Home <span class="sr-only"></span></a></li>\
                      <li class="nav-item" id="benefits"> <a class="nav-link">Benefits</a> </li>\
                  <li class="nav-item" id="about"> <a class="nav-link">About</a> </li>'

  return authenticated ? auth : unauth;
}