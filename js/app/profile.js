import nav from './nav';
import actions from './api';
import admin from './admin';
import manager from './manager';
import router from './router';

export default {
  load(data) {
    if (data['role'] == 'manager') {
      manager.load(data)
    }
    else {
      var arrow = (data['role'] == 'admin') ? '<a id="goAdmin" class="glyphicon glyphicon-indent-right text-success"></a>' : '';
      $('#navbarCollapse1').html(nav(true));
      $('#view').html('<div class="row" style="margin-top: 20%">\
        <div class="portlet">\
        <div class="portlet-title">\
        <div class="caption caption-red">\
        <i class="glyphicon glyphicon-grain"></i>\
        <span class="caption-subject bold font-yellow-crusta uppercase">'+data.user.firstName+' '+ data.user.lastName
        + '  ' +'</span></div> ' + arrow +'<span class="col-md-4 col-md-offset-3">\
        <span action="" class="search-form">\
        <span class="form-group has-feedback">\
        <label for="search" class="sr-only">Search</label>\
        <input type="text" class="form-control" name="search" id="search" placeholder="search">\
        <span class="glyphicon glyphicon-search text-warning form-control-feedback"></span>\
        </span></span></span>\
        <ul class="nav nav-tabs">\
        <li id="all" class="filter active">\
        <a href="#portlet_tab1" data-toggle="tab">All</a>\
        </li>\
        <li id="accepted" class="filter">\
        <a href="#portlet_tab2" data-toggle="tab">Accepted</a>\
        </li>\
        <li id="rejected" class="filter">\
        <a href="#portlet_tab3" data-toggle="tab">Rejected</a>\
        </li>\
        <li id="pending" class="filter">\
        <a href="#portlet_tab4" data-toggle="tab">Pending</a>\
        </li>\
        </ul>\
        <a data-toggle="modal" data-target="#reimbModal" class="glyphicon glyphicon-plus text-success"></a>\
        <a data-toggle="modal" data-target="#configModal" class="glyphicon glyphicon-cog text-warning"></a>\
        <a data-toggle="modal" data-target="#emailModal" class="glyphicon glyphicon-envelope text-danger"></a>\
        </div>\
        <div class="col-xs-3 border col-xs-offset-0" style="margin-top: 25;">\
        <h3 class="side-title">Category</h3>\
        <ul class="single list-unstyled">\
        <li class="mock filter" id="#all"><a>All</a></li>\
        <li class="mock"><a id="business">Business travel</a></li>\
        <li class="mock"><a id="training">Education or training</a></li>\
        <li class="mock"><a id="supplies">Business supplies</a></li>\
        <li class="mock"><a id="tools">Business tools</a></li>\
        <li class="mock"><a id="miscellaneous">Miscellaneous Expenses</a></li>\
        <li class="mock"><a id="medical">Medical Expenses</a></li>\
        </ul>\
        </div>\
        <div class="col-xs-9 border" style="margin-top: 25px;">\
        <div class="category">\
        <div class="tab-content">\
        <div class="tab-pane active" id="portlet_tab1">\
        </div>\
        <div class="tab-pane active" id="portlet_tab2">\
        </div>\
        <div class="tab-pane active" id="portlet_tab3">\
        </div>\
        <div class="tab-pane active" id="portlet_tab4">\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>\
        </div>')
      
      $(function () {
        var imgSrc;
        var updateSrc;
        var all = data['reimbursements']
        var pending = _.filter(all, el => el.status == 'pending')
        var accepted = _.filter(all, el => el.status == 'accepted')
        var rejected = _.filter(all, el => el.status == 'rejected')
        var current = all;
        var curnAll = all;
        var curnPending = pending;
        var curnAccepted = accepted;
        var curnRejected = rejected;
        var deleted;
        var updated;
        console.log(data)
        if (data.role=='manager')
          _.each(data.users, val => {
            $('#userList').append('<ul><li>Username: '+val.username+'</li><li>Name: '+val.firstName 
              + ' ' + val.lastName + '</li><li>Email: '+ val.email)
          })


        function insert (el, lis) {
          $(el).html('')
          _.each(lis, val => $(el).append('<div><p>'+val.reimbursement.description+'</p><span>\
            <a data-toggle="modal" data-target="#reimInfoModal"  id="'+val.reimbursement.id+'" class="mInfo glyphicon glyphicon-stats text-info"></a>\
            <a data-toggle="modal" data-target="#reimUpdateModal"  id="'+val.reimbursement.id+'"class="mUpdate glyphicon glyphicon-edit text-warning"></a>\
            <a data-toggle="modal" data-target="#deleteModal"  id="'+val.reimbursement.id+'"class="mDelete glyphicon glyphicon-trash text-danger"></a>\
            </span></div>')) 
        }

        function filter (el, lis, type) {
          lis = _.filter(lis, val => (val.type == type))
          insert(el, lis)
        }

        function getOption(option) {
          if (option == 'Business travel')
            return 1;
          else if (option == 'Education or training')
            return 2;
          else if (option == 'Business supplies')
            return 3;
          else if (option == 'Business tools')
            return 4;
          else if (option == 'Miscellaneous Expenses')
            return 5;
          else if (option == 'Medical Expenses')
            return 6;
        }


        function init () {
          insert('#portlet_tab1', all)
          insert('#portlet_tab2', accepted)
          insert('#portlet_tab3', rejected)
          insert('#portlet_tab4', pending)
        }

        function find (str) {
          if (str == 'all')
            init()
          else {
            curnRejected = _.filter(rejected, el => el.type == str)
            curnAccepted = _.filter(accepted, el => el.type == str)
            curnPending = _.filter(pending, el => el.type == str)
            curnAll = _.filter(all, el => el.type == str)
            insert('#portlet_tab1', curnAll)
            insert('#portlet_tab2', curnAccepted)
            insert('#portlet_tab3', curnRejected)
            insert('#portlet_tab4', curnPending)
          }
        }

        $('.mock').click(e => find(e.target.id))

        init()

        $('#goAdmin').click(e => {
          admin.load(data)
        })

        $('#search').keypress(e => {
          if ($('#search').val() != '') {
            var searchAll = _.filter(all, val => val.reimbursement.description.includes($('#search').val()))
            var searchRejected = _.filter(curnRejected, val => val.reimbursement.description.includes($('#search').val()))
            var searchAccepted = _.filter(curnAccepted, val => val.reimbursement.description.includes($('#search').val()))
            var searchPending = _.filter(curnPending, val => val.reimbursement.description.includes($('#search').val()))
            insert('#portlet_tab1', searchAll)
            insert('#portlet_tab2', searchAccepted)
            insert('#portlet_tab3', searchRejected)
            insert('#portlet_tab4', searchPending)
          }
          else
            init()
        })

        $('#sendMail').click(e => {
          var value = {
            'sender': data.user.username,
            'subject': $('#subject').val(),
            'text': $('#emailBody').val()
          }
          actions.post('http://localhost:9000/Reimbursement/api/v1/reimbursements', value);
        })

        $('input[type=file]').change(function () {
          actions.makeBlob(this.files[0])
          .then(val => setImg(val))
        })

        $('#addReim').click(e => {
          var value = { 
            'amount': $('#Ramount').val(),
            'description': $('#Rdescription').val(),
            'receipt': imgSrc,
            'author': data.user.id,
            'resolver': 1,
            'type': getOption($('#Rtype').val())
          }
          actions.post('http://localhost:9000/Reimbursement/api/v1/reimbursements', value)
          .then(() => location.reload())
          .catch(err => console.log(err))
        })

        $('.mInfo').click((e) => {
          var id = e.target.id;
          var value = _.find(all, val => (val.reimbursement.id == id))
          console.log(value)
          $('#infoDesc').text('Description: ' + value.reimbursement.description)
          $('#infoStatus').text('Status: '+ value.status)
          $('#infoSubmitted').text('At: ' + value.reimbursement.submitted)
        })

        $('.mDelete').click(e => {
          var id = e.target.id;
          deleted = _.find(all, val => (val.reimbursement.id == id))
        })

        $('.mUpdate').click(e => {
          var id = e.target.id;
          updated = _.find(all, val => (val.reimbursement.id == id))
          $('#Uamount').val(updated.reimbursement.amount)
          // $('#Ureciept').val(updated.receipt)
          $('#Udescription').val(updated.reimbursement.description)
          $('#Utype option[value="'+updated.reimbursement.type+'"]').attr("selected",true)
        })

        $('#deleteReim').click(e => {
          _.remove(all, x => x.id == deleted.id)
          actions.delete('http://localhost:9000/Reimbursement/api/v1/reimbursements', deleted.reimbursement)
          .then(() => location.reload())
          .catch(err => console.log(err))
        })

        $('#updateReim').click(e => {
          updated.reimbursement.amount = $('#Uamount').val()
          updated.reimbursement.receipt = imgSrc
          updated.reimbursement.description = $('#Udescription').val()
          updated.reimbursement.type = getOption($('#Utype').val())

          actions.put('http://localhost:9000/Reimbursement/api/v1/reimbursements', updated.reimbursement)
          .then(() => location.reload())
          .catch(err => console.log(err))
        })

        $('#updateUser').click(e => {
          data.user.email = $('#cuemail').val()
          data.user.password = $('#cupassword').val()
          actions.put('http://localhost:9000/Reimbursement/api/v1/users', data.user)
          .then(() => location.reload())
          .catch(e => console.log(e))        
        })

        $('#logout').click(function (e) {
          actions.logout('/Reimbursement/api/v1/logout')
          .then(() => location.reload())
          .catch(err => console.log(err))
        })

        $(".nav-item").click(function (e) {
          $(".nav-item").removeClass("active");
          console.log($(this).attr('id'))
          $(this).addClass("active");
          console.log($(this).attr('id'));
          $('#view').html(router.redirect({'url': $(this).attr('id')}))
          // location.reload();
        })
      })
    }
  }
}