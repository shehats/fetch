import login from './login';
import profile from './profile';;
import lost from './lost';
import about from './about';
import benefits from './benefits';

export default {
  redirect (res) {
    if (res['url'] == 'login')
      location.reload()
    else if (res['url'] == 'profile')
      location.reload()
    else if (res['url'] == 'about')
      return about();
    else if (res['url'] == 'benefits')
      return benefits();
    else
      return lost();
  }
}
