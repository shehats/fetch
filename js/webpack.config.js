const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var $ = require('jquery');
var _ = require('lodash')
module.exports = {
	entry: './app/index.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
  module: {
   loaders: [
   {
     test: /\.js$/,
     loader: './node_modules/babel-loader',
     query: {
       presets: ['es2015']
     }
   }
   ]
 },
 plugins: [
  new UglifyJSPlugin()
 ]
}
